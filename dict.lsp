#!/usr/bin/newlisp

(define (get-word-xml word)
  (letn ((api-url "http://dict-co.iciba.com/api/dictionary.php?w=")
	 (word-url (append api-url word))
	 (xml (get-url word-url 3000)))
    (xml-type-tags nil nil nil nil)
    (xml-parse xml (+ 1 2 4))))

(define (show-entry entries)
  (letn ((tags '(("key" "单词:" "\n")("ps" "音标:[" "]\n")("pos" "" " ")("acceptation" "\t" "")("sent" "" "")("orig" "\n原句:" "")("trans" "翻译:" "")))
	 (res ""))
    (dolist (item entries)
	    (let ((head (assoc (first item) tags)))
	      (if head
		  (if (= (first item) "sent")
		      (extend res (head 1) (head 2) (show-entry (rest item)))
		      (extend res (head 1) (join (flat (rest item))) (head 2))))))
    res))

(define (show-word word-xml)
  (let ((res ""))
    (dolist (entry word-xml)
	    (extend res (show-entry entry)))
    res))

(define (main)
  (let ((words (2 (main-args)))) 
    (if words
	(print (show-word (get-word-xml (join words " "))))
	(print "Usage: dict.lsp [word]\n"))))

(main)
(exit)
; EOF
